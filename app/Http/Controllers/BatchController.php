<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\BatchType;
use App\Models\Batch;
use App\Models\Warehouse;
use DB;
use App\Models\StatusLog;
use App\Http\Controllers\WarehouseController;

class BatchController extends WarehouseController
{
    //

    public function fetchStatuses(){
        return BatchType::all();
    }

    public function createBatch(Request $request, Batch $log){

        $rules = [
            'cycle_id' => ['required'],
            'type' => ['required'],
            'notes' => ['required'],
            'quantity' => ['required'],
            'option' => ['required'],
        ];

     
        if($request->input('option') == "probiotics") $rules['warehouse_id'] = ['required'];
     
        $request->validate($rules);


        // try {
          
            $data = $request->only($log->getFillable());
            
            $log = $log::create($data);

            if($log) {
                $this->createLog($request);
            }
          
            $data = [
                'success' => true,
                'message'=> 'Added Log!'
              ] ;
              
              return response()->json($data);
        // } catch (\Throwable $th) {
        //     $data = [
        //         'success' => false,
        //         'message'=> 'Error Occured!'
        //       ] ;
              
        //       return response()->json($data);
        // }
    }

    public function updateWarehouse($id, $quantity){
        
    }

    public function fetchBatches(Request $request, $id){

        $batch = new Batch;
        
     
        $this->counts($id, "partial harvest", $batch);

        $probiotics = $batch->with('cycle.tank', 'type')->where('cycle_id', $id)->where('option', 'probiotics');
        
        // ->orderByRaw('updated_at - created_at DESC')
        $batches = $probiotics->paginate(10, '*', 'page', $request->current_page);
        
        $total_quantity = $probiotics->selectRaw('sum(quantity) as total_quantity')->pluck('total_quantity')->first();
        $total_harvest = $this->counts($id, "harvest", $batch);
        $total_partial_harvest = $this->counts($id, "partial harvest", $batch);
        
        $transfer = $this->counts($id, "transfer", $batch);

        $logs = StatusLog::with('status')->where('cycle_id', $id)->get();
      

        return ['batches' => $batches, 'total_quantity' => $total_quantity, 'total_harvest' => $total_harvest, 'total_partial_harvest' => $total_partial_harvest, 'transfer' => $transfer, 'logs' => $logs];
    }

    public function counts($id, $type, $batch){
        
        $transfer = $batch->with('cycle.tank', 'type')->where(function ($query) use ($id, $type){ // Wrap you code inside a where
            $query->where('cycle_id', $id)->whereHas('type', function ($query) use ($type) {
                return $query->where('name', '=', $type);
            })->where('option', 'shirmp');
        });

        return $transfer->selectRaw('sum(quantity) as count')->pluck('count')->first();
    }
}
