<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cycle;
use App\Models\CycleStatus;
use App\Models\Tank;

class CycleController extends Controller
{
    //

    public function create(Request $request){
        $data = $request->all();
        $cycle = Cycle::create(['tank_id' => $data['id'], "status" => 1]);
        Tank::where('id', $data['id'])->update(['active_cycle' => $cycle->id]);
        
        $data = [
            'success' => true,
            'message'=> 'Created new Cycle'
        ];
          
        return response()->json($data);
    }

    public function fetchStatuses(){
        return CycleStatus::all();
    }
}
