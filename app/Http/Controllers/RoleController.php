<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Models\User;

class RoleController extends Controller
{
    //

    public function create($type) {
        $role = Role::create(['name' => $type]);
    }

    public function assign($id, $role) {
        $user= User::where('id', $id)->first();
        // $role = Role::findByName($role);
        $user->assignRole($role);
    }
}
