<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Tank;
use App\Models\StatusLog;
use App\Models\CycleStatus;
use App\Models\BatchType;
use App\Models\Cycle;

class TankController extends Controller
{
    //

    public function fetch(){
        
        $batchTypes = BatchType::whereIn('name',['harvest', 'partial harvest', 'transfer'])->get()->pluck('id');
        
        $tank = Tank::with('cycle.status')->with('cycle')->orderBy('id')->get()->transform(function($q) use ($batchTypes) {
            if(!$q['active_cycle']) return $q;
          
            $q['probiotics'] = Cycle::where('id', $q->active_cycle)->whereHas('batches')->withSum(['batches' => function ($query){
                $query->where('option', 'probiotics');
            }], 'quantity')->first();

            // if($q['probiotics'] && $q['probiotics']->toArray()) {
            //     $q['probiotics_date'] =  $q['probiotics']->toArray()['last_batch']['updated_at'] ?? "";
            // }

            $q['probiotics'] = $q['probiotics']['batches_sum_quantity'] ?? 0;

            $q['shirmps'] = Cycle::where('id', $q->active_cycle)->whereHas('batches')->withSum(['batches' => function ($query) use ($batchTypes){
                $query->where('option', 'shirmp')->whereIn('type', $batchTypes);
            }], 'quantity')->first();
            
            // if($q['shirmps'] && $q['shirmps']->toArray()) {
            //     $q['shirmps_date'] =  $q['shirmps']->toArray()['last_batch']['updated_at'] ?? "";
            // }

            $q['shirmps'] = $q['shirmps']['batches_sum_quantity'] ?? 0;

            return $q;
        });
          
            
        return $tank;
    }

    public function updateStatus(Request $request, StatusLog $log) {
       
        $data = $request->only($log->getFillable());
       

        $status = CycleStatus::where('sort', (++$request->sort))->pluck('id')->first();
        $cycle = Cycle::where('id', $data['cycle_id'])->first();
        if(!$status) {
            $status = CycleStatus::find($data['status_id']);
            if($status->name == 'harvest') {
                // $cycle->tank->update("active_cycle", null);

                $cycle->tank->active_cycle = null;
 
                $cycle->tank->save();
            }
        } else {
            
            $cycle->update(["status" => (string)$status]);
            $data['status_id'] = $status;
            $log = $log::updateOrCreate(["status_id" => $data['status_id'], "cycle_id" => $data['cycle_id']], $data);
        }
      

        $data = [
            'success' => true,
            'message'=> 'Update Tank Status!'
        ];

        return response()->json($data);
    }
}
