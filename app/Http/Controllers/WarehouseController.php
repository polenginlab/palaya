<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Warehouse;
use App\Models\WarehouseLog;
use Illuminate\Validation\ValidationException;

class WarehouseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function fetch(){
        return Warehouse::all();
    }

    public function probiotics(){
        return Warehouse::first();
    }

    public function createLog(Request $request){
      
        $request->validate([
            'quantity' => ['required', 'numeric'],
        ]);
       
        $log = new WarehouseLog;

        try {
          
            $data = $request->only($log->getFillable());

            if($data['log_type'] == 'minus') $data['quantity'] = "-".$data['quantity'];
            
            $data["quantity"] = (float)$data["quantity"];
            $log = $log::create($data);

            if($request->input('unit') == 'SACK' && $request->input('quantity') < 0) {
                $data = [
                    'success' => false,
                    'message'=> 'Cannot put minus Sack Unit!'
                ] ;
                return response()->json($data);
            }

            $warehouse = Warehouse::where("id", $data['warehouse_id']);
            $warehouseData = $warehouse->first();
            $total_quantity = $data['quantity'] + $warehouseData['quantity'];
          
            if($total_quantity < 0) {

                $data = [
                    'success' => false,
                    'message'=> 'This will make the quantity negative!'
                ] ;
                return response()->json($data);
            }

            $warehouse = $warehouse->update(['quantity' => $total_quantity]);
            
            $data = [
                'success' => true,
                'message'=> 'Added Log!'
              ] ;
              
              return response()->json($data);
        } catch (\Throwable $th) {
            $data = [
                'success' => false,
                'message'=> 'Error Occured!'
              ] ;
              
              return response()->json($data);
        }
    }

    public function logs(Request $request, $id){
        return WarehouseLog::where('warehouse_id', $id)->orderBy('created_at', 'desc')->paginate(10, '*', 'page', $request->current_page);
    }
}
