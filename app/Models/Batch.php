<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Batch extends Model
{
    use HasFactory;

    protected $fillable = [
        'quantity',
        'option',
        'cycle_id',
        'type',
        'notes',
        'from_id',
    ];

    public function cycle()
    {
        return $this->hasOne('App\Models\Cycle', 'id', 'from_id');
    }

    public function type()
    {
        return $this->hasOne('App\Models\BatchType', 'id', 'type');
    }
}
