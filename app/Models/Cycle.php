<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cycle extends Model
{
    use HasFactory;
    protected $guarded;

    public function status()
    {
        return $this->hasOne('App\Models\CycleStatus', 'id', 'status');
    }

    public function tank()
    {
        return $this->hasOne('App\Models\Tank', 'id', 'tank_id');
    }

    public function batches()
    {
        return $this->hasMany('App\Models\Batch', 'cycle_id', 'id');
    }

    public function lastBatch()
    {
        return $this->hasOne('App\Models\Batch', 'cycle_id', 'id')->latest();
    }
}
