<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CycleStatus extends Model
{
    use HasFactory;
    protected $table = "cycle_statuses";
}
