<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StatusLog extends Model
{
    use HasFactory;
    protected $fillable = [
        'cycle_id',
        'status_id',
        'notes',
    ];

    public function status()
    {
        return $this->hasOne('App\Models\CycleStatus', 'id', 'status_id');
    }

}
