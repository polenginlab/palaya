<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tank extends Model
{
    use HasFactory;

    public function cycle()
    {
        return $this->hasOne('App\Models\Cycle', 'id', 'active_cycle');
    }
}
