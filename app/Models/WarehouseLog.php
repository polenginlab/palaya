<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WarehouseLog extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'warehouse_id',
        'current_stock',
        'quantity',
        'notes',
        'log_type',
    ];

}
