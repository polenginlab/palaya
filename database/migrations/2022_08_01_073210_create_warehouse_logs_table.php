<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('warehouse_logs', function (Blueprint $table) {
            $table->id();
            $table->integer("warehouse_id");
            $table->double('current_stock', 8, 2)->default(0.00);
            $table->double('quantity', 8, 2)->default(0.00);
            $table->text("notes")->nullable();
            $table->string("log_type");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('warehouse_logs');
    }
};
