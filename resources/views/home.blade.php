
@extends('layouts.app')

@section('content')
    <pagecomponent></pagecomponent>
@endsection

@section('sub_component')
    
<div class="d-flex flex-column flex-column-fluid">
    <!--begin::Toolbar-->
    <template v-if="!is_update">
        <div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
        <!--begin::Toolbar container-->
            <div id="kt_app_toolbar_container" class="app-container container-xxl d-flex flex-stack" v-if="false">
                <!--begin::Page title-->
                <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3" >
                    <!--begin::Title-->
                    <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">Mixed</h1>
                    <!--end::Title-->
                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                        <!--begin::Item-->
                        <li class="breadcrumb-item text-muted">
                            <a href="../../demo1/dist/index.html" class="text-muted text-hover-primary">Home</a>
                        </li>
                        <!--end::Item-->
                        <!--begin::Item-->
                        <li class="breadcrumb-item">
                            <span class="bullet bg-gray-400 w-5px h-2px"></span>
                        </li>
                        <!--end::Item-->
                        <!--begin::Item-->
                        <li class="breadcrumb-item text-muted">Widgets</li>
                        <!--end::Item-->
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page title-->
                <!--begin::Actions-->
                <div class="d-flex align-items-center gap-2 gap-lg-3">
                    <!--begin::Filter menu-->
                    <div class="m-0">
                        <!--begin::Menu toggle-->
                        <a href="#" class="btn btn-sm btn-flex bg-body btn-color-gray-700 btn-active-color-primary fw-bold" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">
                        <!--begin::Svg Icon | path: icons/duotune/general/gen031.svg-->
                        <span class="svg-icon svg-icon-6 svg-icon-muted me-1">
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M19.0759 3H4.72777C3.95892 3 3.47768 3.83148 3.86067 4.49814L8.56967 12.6949C9.17923 13.7559 9.5 14.9582 9.5 16.1819V19.5072C9.5 20.2189 10.2223 20.7028 10.8805 20.432L13.8805 19.1977C14.2553 19.0435 14.5 18.6783 14.5 18.273V13.8372C14.5 12.8089 14.8171 11.8056 15.408 10.964L19.8943 4.57465C20.3596 3.912 19.8856 3 19.0759 3Z" fill="currentColor" />
                            </svg>
                        </span>
                        <!--end::Svg Icon-->Filter</a>
                        <!--end::Menu toggle-->
                        <!--begin::Menu 1-->
                        <div class="menu menu-sub menu-sub-dropdown w-250px w-md-300px" data-kt-menu="true" id="kt_menu_62c3e3e13ab06">
                            <!--begin::Header-->
                            <div class="px-7 py-5">
                                <div class="fs-5 text-dark fw-bold">Filter Options</div>
                            </div>
                            <!--end::Header-->
                            <!--begin::Menu separator-->
                            <div class="separator border-gray-200"></div>
                            <!--end::Menu separator-->
                            <!--begin::Form-->
                            <div class="px-7 py-5">
                                <!--begin::Input group-->
                                <div class="mb-10">
                                    <!--begin::Label-->
                                    <label class="form-label fw-semibold">Status:</label>
                                    <!--end::Label-->
                                    <!--begin::Input-->
                                    <div>
                                        <select class="form-select form-select-solid" data-kt-select2="true" data-placeholder="Select option" data-dropdown-parent="#kt_menu_62c3e3e13ab06" data-allow-clear="true">
                                            <option></option>
                                            <option value="1">Approved</option>
                                            <option value="2">Pending</option>
                                            <option value="2">In Process</option>
                                            <option value="2">Rejected</option>
                                        </select>
                                    </div>
                                    <!--end::Input-->
                                </div>
                                <!--end::Input group-->
                                <!--begin::Input group-->
                                <div class="mb-10">
                                    <!--begin::Label-->
                                    <label class="form-label fw-semibold">Member Type:</label>
                                    <!--end::Label-->
                                    <!--begin::Options-->
                                    <div class="d-flex">
                                        <!--begin::Options-->
                                        <label class="form-check form-check-sm form-check-custom form-check-solid me-5">
                                            <input class="form-check-input" type="checkbox" value="1" />
                                            <span class="form-check-label">Author</span>
                                        </label>
                                        <!--end::Options-->
                                        <!--begin::Options-->
                                        <label class="form-check form-check-sm form-check-custom form-check-solid">
                                            <input class="form-check-input" type="checkbox" value="2" checked="checked" />
                                            <span class="form-check-label">Customer</span>
                                        </label>
                                        <!--end::Options-->
                                    </div>
                                    <!--end::Options-->
                                </div>
                                <!--end::Input group-->
                                <!--begin::Input group-->
                                <div class="mb-10">
                                    <!--begin::Label-->
                                    <label class="form-label fw-semibold">Notifications:</label>
                                    <!--end::Label-->
                                    <!--begin::Switch-->
                                    <div class="form-check form-switch form-switch-sm form-check-custom form-check-solid">
                                        <input class="form-check-input" type="checkbox" value="" name="notifications" checked="checked" />
                                        <label class="form-check-label">Enabled</label>
                                    </div>
                                    <!--end::Switch-->
                                </div>
                                <!--end::Input group-->
                                <!--begin::Actions-->
                                <div class="d-flex justify-content-end">
                                    <button type="reset" class="btn btn-sm btn-light btn-active-light-primary me-2" data-kt-menu-dismiss="true">Reset</button>
                                    <button type="submit" class="btn btn-sm btn-primary" data-kt-menu-dismiss="true">Apply</button>
                                </div>
                                <!--end::Actions-->
                            </div>
                            <!--end::Form-->
                        </div>
                        <!--end::Menu 1-->
                    </div>
                    <!--end::Filter menu-->
                    <!--begin::Secondary button-->
                    <!--end::Secondary button-->
                    <!--begin::Primary button-->
                    <a href="#" class="btn btn-sm fw-bold btn-primary" data-bs-toggle="modal" data-bs-target="#kt_modal_create_app">Create</a>
                    <!--end::Primary button-->
                </div>
                <!--end::Actions-->
            </div>
            <!--end::Toolbar container-->
        </div>
        <!--end::Toolbar-->
        <!--begin::Content-->
        <div id="kt_app_content" class="app-content flex-column-fluid">
            <!--begin::Content container-->
            <div id="kt_app_content_container" class="app-container container-xxl" >
                <!--begin::Row-->
    
                <div class="card border-transparent mb-5 mb-xl-10" data-theme="light" style="background-color: #1C325E;">
                    <!--begin::Body-->
                    
                    <div class="card-body d-flex ps-xl-15">
                        <!--begin::Wrapper-->
                        <div class="m-0">
                            <!--begin::Title-->
                            <h3 class="card-title align-items-start flex-column text-white pt-15">
                                <span class="fw-bold fs-2x mb-3 opacity-75">ANNOUNCEMENTS</span>
                                
                            </h3>
                            <div class="position-relative fs-2x z-index-2 fw-bold text-white mb-7">
                            <span class="me-2">Add announcement
                            <span class="position-relative d-inline-block text-danger">
                                <!--begin::Separator-->
                                <span class="position-absolute opacity-50 bottom-0 start-0 border-4 border-danger border-bottom w-100"></span>
                                <!--end::Separator-->
                            </span></span>
                            <br></div>
                            <!--end::Title-->
                            <!--begin::Action-->
                            <div class="mb-3" v-if="false">
                                <a href="#" class="btn btn-danger fw-semibold me-2" data-bs-toggle="modal" data-bs-target="#kt_modal_upgrade_plan">Get Reward</a>
                                <a href="../../demo1/dist/apps/support-center/overview.html" class="btn btn-color-white bg-white bg-opacity-15 bg-hover-opacity-25 fw-semibold">How to</a>
                            </div>
                            <!--begin::Action-->
                        </div>
                        <!--begin::Wrapper-->
                        <!--begin::Illustration-->
                        {{-- <img src="assets/media/illustrations/sigma-1/17-dark.png" class="position-absolute me-3 bottom-0 end-0 h-200px" alt=""> --}}
                        <!--end::Illustration-->
                    </div>
                    <!--end::Body-->
                </div>
    
                <div class="row g-5 g-xl-10 mb-5 mb-xl-10" >
                    <!--begin::Col-->
                    <div class="col-xxl-6 col-md-6" v-for="(tank, index) in tanks">
                        <!--begin::Card widget 18-->
                        <div class="card card-flush h-xl-100" >
                
                            <div class="card-header pt-5">
                                                
                                <!--begin::Title-->
                                <h4 class="card-title d-flex align-items-start flex-column">
                                    <span class="card-label fw-bold text-gray-800 text-uppercase">@{{ tank.name }}</span>
                                    <span class="text-gray-400 mt-1 fw-bold fs-7">Cycle#: 0000@{{ tank?.cycle?.id }}</span>
                                </h4>
                                <!--end::Title-->
                                <!--begin::Toolbar-->
                                <div class="card-toolbar">
                                    <!--begin::Carousel Indicators-->
                                    
                                    <span class="badge py-3 px-4 fs-7 text-capitalize" :class="'badge-light-'+tank.cycle?.status.css" v-if="tank.cycle">@{{ tank.cycle?.status.name }}</span>
                                    <span class="badge py-3 px-4 fs-7 text-capitalize badge-light-white" v-else>No Cycle</span>
                                    <!--end::Carousel Indicators-->
                                </div>
                                <!--end::Toolbar-->
                            </div>
                            <!--begin::Body-->
                            <div class="card-body py-9">
                                <!--begin::Carousel-->
                                <div class="carousel-inner">
                                    <!--begin::Item-->
                                    <div class="carousel-item active show">
                                        <!--begin::Wrapper-->
                                        <div class="d-flex flex-wrap gap-10 align-items-center mb-9 ">
                                            <!--begin::Symbol-->
                                            
                                            <!--end::Symbol-->
                                            <!--begin::Info-->
                                            <div class="m-0 flex-column-fluid ">
                                                <!--begin::Subtitle-->
                                                <h4 class="fw-bold text-gray-800 mb-3">Status</h4>
                                                <!--begin::Item-->
                                                    <div class="d-flex flex-stack">
                                                        <!--begin::Section-->
                                                        <div class="d-flex align-items-center me-5">
                                                            <!--begin::Symbol-->
                                                            <div class="symbol symbol-40px me-4">
                                                                <span class="symbol-label">
                                                                    
                                                                    <i class="fa-solid fa-shrimp fs-1 p-0 text-gray-600"></i>
                                                                </span>
                                                            </div>
                                                            <!--end::Symbol-->
                                                            <!--begin::Content-->
                                                            <div class="me-5">
                                                                <!--begin::Title-->
                                                                <a href="#" class="text-gray-800 fw-bold text-hover-primary fs-6">Shrimps</a>
                                                                <!--end::Title-->
                                                                <!--begin::Desc-->
                                                                {{-- <span class="text-gray-400 fw-semibold fs-7 d-block text-start ps-0">@{{ tank.shirmps_date | dateConvert }}</span> --}}
                                                                <!--end::Desc-->
                                                            </div>
                                                            <!--end::Content-->
                                                        </div>
                                                        <!--end::Section-->
                                                        <!--begin::Wrapper-->
                                                        <div class="text-gray-400 fw-bold fs-7 text-end">
                                                        <!--begin::Number-->
                                                        <span class="text-gray-800 fw-bold fs-6 d-block">@{{ tank.shirmps ?? 0 }}</span>
                                                        <!--end::Number-->KG</div>
                                                        <!--end::Wrapper-->
                                                    </div>
                                                    <!--end::Item-->
                                                    <!--begin::Separator-->
                                                    <div class="separator separator-dashed my-5"></div>
                                                    <!--end::Separator-->
                                                    <!--begin::Item-->
                                                    <div class="d-flex flex-stack">
                                                        <!--begin::Section-->
                                                        <div class="d-flex align-items-center me-5">
                                                            <!--begin::Symbol-->
                                                            <div class="symbol symbol-40px me-4">
                                                                <span class="symbol-label">
                                                                    
                                                                    <i class="fa-solid fa-bowl-food fs-1 p-0 text-gray-600"></i>
                                                                </span>
                                                            </div>
                                                            <!--end::Symbol-->
                                                            <!--begin::Content-->
                                                            <div class="me-5">
                                                                <!--begin::Title-->
                                                                <a href="#" class="text-gray-800 fw-bold text-hover-primary fs-6">Probiotics</a>
                                                                <!--end::Title-->
                                                                <!--begin::Desc-->
                                                                {{-- <span class="text-gray-400 fw-semibold fs-7 d-block text-start ps-0">@{{ tank.probiotics_date | dateConvert }}</span> --}}
                                                                <!--end::Desc-->
                                                            </div>
                                                            <!--end::Content-->
                                                        </div>
                                                        <!--end::Section-->
                                                        <!--begin::Wrapper-->
                                                        <div class="text-gray-400 fw-bold fs-7 text-end">
                                                        <!--begin::Number-->
                                                        <span class="text-gray-800 fw-bold fs-6 d-block">@{{ tank.probiotics ?? 0 }}</span>
                                                        <!--end::Number-->KG</div>
                                                        <!--end::Wrapper-->
                                                    </div>
                                                
                                                    
                                            </div>
                            
                                            <div class="symbol symbol-70px symbol-circle">
                                                <span class="symbol-label" :class="'bg-light-'+tank.cycle?.status.css">
                            
                                                    <!--begin::Svg Icon | path: icons/duotune/abstract/abs025.svg-->
                                                    <span class="svg-icon svg-icon-3x" :class="'svg-icon-'+tank.cycle?.status.css">
                            
                                                        <div v-html="tank.cycle?.status.svg" v-if="tank.cycle"></div>
                                                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" v-else>
                                                            <path opacity="0.3" d="M2.10001 10C3.00001 5.6 6.69998 2.3 11.2 2L8.79999 4.39999L11.1 7C9.60001 7.3 8.30001 8.19999 7.60001 9.59999L4.5 12.4L2.10001 10ZM19.3 11.5L16.4 14C15.7 15.5 14.4 16.6 12.7 16.9L15 19.5L12.6 21.9C17.1 21.6 20.8 18.2 21.7 13.9L19.3 11.5Z" fill="currentColor"/>
                                                            <path d="M13.8 2.09998C18.2 2.99998 21.5 6.69998 21.8 11.2L19.4 8.79997L16.8 11C16.5 9.39998 15.5 8.09998 14 7.39998L11.4 4.39998L13.8 2.09998ZM12.3 19.4L9.69998 16.4C8.29998 15.7 7.3 14.4 7 12.8L4.39999 15.1L2 12.7C2.3 17.2 5.7 20.9 10 21.8L12.3 19.4Z" fill="currentColor"/>
                                                        </svg>
                                                    </span>
                                                    <!--end::Svg Icon-->
                                                </span>
                                            </div>
                                            <!--end::Info-->
                                        </div>
                                        <!--end::Wrapper-->
                                        <!--begin::Action-->
                                        <div class="mb-1 flex-column-fluid">
          
                                            <div class="submit-button" style="display: inline-block" v-if="tank.cycle && !['CLEANING', 'DRYING'].includes(tank.cycle?.status?.name.toUpperCase())">
                                                <button type="button" class="btn btn-sm btn-light me-2" @click.prevent="update($event, tank)">
                                                    
                                                    <span class="indicator-label">Update
                                                    <!--begin::Svg Icon | path: icons/duotune/arrows/arr064.svg-->
                                                    </span>
                                                    <span class="indicator-progress">Please wait...
                                                    <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                                                </button>
                                            </div>
                                            
                                            <div class="submit-button" style="display: inline-block">
                                                <button type="button" class="btn btn-sm btn-success" @click.prevent="updateStatus($event, tank)" v-if="tank.cycle">
                                                    <span class="indicator-label">Continue to <span class="text-capitalize">@{{ cycle_statuses[tank.cycle?.status?.sort] ? cycle_statuses[tank.cycle?.status?.sort]?.name : "Close Cycle" }}</span>
                                                    <!--begin::Svg Icon | path: icons/duotune/arrows/arr064.svg-->
                                                    <span class="svg-icon svg-icon-3 ms-2 me-0">
                                                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                            <rect opacity="0.5" x="18" y="13" width="13" height="2" rx="1" transform="rotate(-180 18 13)" fill="currentColor"></rect>
                                                            <path d="M15.4343 12.5657L11.25 16.75C10.8358 17.1642 10.8358 17.8358 11.25 18.25C11.6642 18.6642 12.3358 18.6642 12.75 18.25L18.2929 12.7071C18.6834 12.3166 18.6834 11.6834 18.2929 11.2929L12.75 5.75C12.3358 5.33579 11.6642 5.33579 11.25 5.75C10.8358 6.16421 10.8358 6.83579 11.25 7.25L15.4343 11.4343C15.7467 11.7467 15.7467 12.2533 15.4343 12.5657Z" fill="currentColor"></path>
                                                        </svg>
                                                    </span>
                                                    <!--end::Svg Icon--></span>
                                                    <span class="indicator-progress">Please wait...
                                                    <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                                                </button>

                                                <button type="button" class="btn btn-sm btn-primary" @click.prevent="createCycle($event, tank)" v-else>
                                                    <span class="indicator-label">Create <span class="text-capitalize">Cycle</span>
                                                    <!--begin::Svg Icon | path: icons/duotune/arrows/arr064.svg-->
                                                    <span class="svg-icon svg-icon-3 ms-2 me-0">
                                                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                            <rect opacity="0.5" x="18" y="13" width="13" height="2" rx="1" transform="rotate(-180 18 13)" fill="currentColor"></rect>
                                                            <path d="M15.4343 12.5657L11.25 16.75C10.8358 17.1642 10.8358 17.8358 11.25 18.25C11.6642 18.6642 12.3358 18.6642 12.75 18.25L18.2929 12.7071C18.6834 12.3166 18.6834 11.6834 18.2929 11.2929L12.75 5.75C12.3358 5.33579 11.6642 5.33579 11.25 5.75C10.8358 6.16421 10.8358 6.83579 11.25 7.25L15.4343 11.4343C15.7467 11.7467 15.7467 12.2533 15.4343 12.5657Z" fill="currentColor"></path>
                                                        </svg>
                                                    </span>
                                                    <!--end::Svg Icon--></span>
                                                    <span class="indicator-progress">Please wait...
                                                    <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                                                </button>

                                            </div>
                                        </div>
                                        <!--end::Action-->
                                    </div>
                                </div>
                                <!--end::Carousel-->
                            </div>
                            <!--end::Body-->
                        </div>
                        <!--end::Card widget 18-->
                    </div>
                    <!--end::Col-->
                    <!--begin::Col-->
                    
                </div>
    
                
    
            
            </div>
            <!--end::Content container-->
        </div>
    </template>

    <template v-else>
        <div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
            <!--begin::Toolbar container-->
            <div id="kt_app_toolbar_container" class="app-container container-xxl d-flex flex-stack">
                <!--begin::Page title-->
                <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3" >
                    <!--begin::Title-->
                    <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">@{{ active_tank.name }}</h1>
                    <!--end::Title-->
                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                        <!--begin::Item-->
                        <li class="breadcrumb-item text-muted">
                            <a href="../../demo1/dist/index.html" class="text-muted text-hover-primary">Cycle#: 0000@{{ active_tank?.cycle?.id }}</a>
                        </li>
                        <!--end::Item-->
                 
                        <!--end::Item-->
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page title-->
                <!--begin::Actions-->
                <div class="d-flex align-items-center gap-2 gap-lg-3">
                    <!--begin::Secondary button-->
                    <!--end::Secondary button-->
                    <!--begin::Primary button-->
                    <button type="button" class="btn btn-light-primary me-3" data-kt-stepper-action="previous" @click="fetchTanks(), is_update = false">
                        <!--begin::Svg Icon | path: icons/duotune/arrows/arr063.svg-->
                        <span class="svg-icon svg-icon-4 me-1">
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <rect opacity="0.5" x="6" y="11" width="13" height="2" rx="1" fill="currentColor"></rect>
                                <path d="M8.56569 11.4343L12.75 7.25C13.1642 6.83579 13.1642 6.16421 12.75 5.75C12.3358 5.33579 11.6642 5.33579 11.25 5.75L5.70711 11.2929C5.31658 11.6834 5.31658 12.3166 5.70711 12.7071L11.25 18.25C11.6642 18.6642 12.3358 18.6642 12.75 18.25C13.1642 17.8358 13.1642 17.1642 12.75 16.75L8.56569 12.5657C8.25327 12.2533 8.25327 11.7467 8.56569 11.4343Z" fill="currentColor"></path>
                            </svg>
                        </span>
                        <!--end::Svg Icon-->Back</button>
                    <!--end::Primary button-->
                </div>
                <!--end::Actions-->
            </div>
            <!--end::Toolbar container-->
        </div>
        <!--end::Toolbar-->
        <!--begin::Content-->
        <div id="kt_app_content" class="app-content flex-column-fluid">
            <!--begin::Content container-->
            <div id="kt_app_content_container" class="app-container container-xxl" >
                <!--begin::Row-->
    
                <div class="row gy-5 g-xl-10">
                    <!--begin::Col-->
    
                    <div class="col-xl-4">
                        <div class="row gy-5 g-xl-10">
                            <!--begin::Col-->
                            <div class="col-xxl-12">
                                <!--begin::Row-->
                                <div class="row gx-5 gx-xl-10">
                                    <!--begin::Col-->
                                    <div class="col-sm-12 mb-5 mb-xl-10">
                                        <!--begin::List widget 1-->
                                        <div class="card card-flush h-lg-100">
                                            <!--begin::Header-->
                                            <div class="card-header pt-5">
                                                <!--begin::Title-->
                                                <h3 class="card-title align-items-start flex-column">
                                                    <span class="card-label fw-bold text-dark">Shirmp Analytics</span>
                                                    {{-- <span class="text-gray-400 mt-1 fw-semibold fs-6">Latest social statistics</span> --}}
                                                </h3>
                                                <!--end::Title-->
                                                <!--begin::Toolbar-->
                                                <div class="card-toolbar">
                                                    <!--begin::Menu-->

                                                    <b-dropdown variant="btn btn-sm btn-light">
                                                        <template #button-content class="">
                                                         Actions
                                                        </template>
                                                        <b-dropdown-item data-bs-toggle="modal" data-bs-target="#add_log" @click="batch.option = 'shirmp'">Add new log</b-dropdown-item>
                                                        {{-- <b-dropdown-item>Second Action</b-dropdown-item> --}}
                                                    </b-dropdown>
                                                    {{-- <a href="#"
                                                    class="btn btn-light btn-sm" data-bs-toggle="modal" data-bs-target="#add_log" @click="batch.option = 'shirmp'">
                                                        <!--end::Search-->Add new log
                                                    </a>
                                                  --}}
                                                </div>
                                                <!--end::Toolbar-->
                                            </div>
                                            <!--end::Header-->
                                            <!--begin::Body-->
                                            <div class="card-body pt-5">
                                                <!--begin::Item-->
                                                <div class="d-flex flex-stack">
                                                    <!--begin::Section-->
                                                    <div class="text-gray-700 fw-semibold fs-6 me-2">Harvest</div>
                                                    <!--end::Section-->
                                                    <!--begin::Statistics-->
                                                    <div class="d-flex align-items-senter">
                                                        <span class="text-gray-900 fw-bolder fs-6">@{{ total_harvest ?? 0 }}kg</span>
                                                    </div>
                                                    <!--end::Statistics-->
                                                </div>
                                                <!--end::Item-->
                                                <!--begin::Separator-->
                                                <div class="separator separator-dashed my-3"></div>
                                                <!--end::Separator-->
                                                <!--begin::Item-->
                                                <div class="d-flex flex-stack">
                                                    <!--begin::Section-->
                                                    <div class="text-gray-700 fw-semibold fs-6 me-2">Partial Harvest</div>
                                                    <!--end::Section-->
                                                    <!--begin::Statistics-->
                                                    <div class="d-flex align-items-senter">
                                                     
                                                        <span class="text-gray-900 fw-bolder fs-6">@{{ total_partial_harvest ?? 0 }}kg</span>
                                                        <!--end::Number-->
                                                    </div>
                                                    <!--end::Statistics-->
                                                </div>
                                                <!--end::Item-->
                                                <!--begin::Separator-->
                                                <div class="separator separator-dashed my-3"></div>
                                                <!--end::Separator-->
                                                <!--begin::Item-->
                                                <div class="d-flex flex-stack">
                                                    <!--begin::Section-->
                                                    <div class="text-gray-700 fw-semibold fs-6 me-2">Transfered</div>
                                                    <!--end::Section-->
                                                    <!--begin::Statistics-->
                                                    <div class="d-flex align-items-senter">
                                                        <!--begin::Svg Icon | path: icons/duotune/arrows/arr094.svg-->
                                                       
                                                        <!--end::Svg Icon-->
                                                        <!--begin::Number-->
                                                        <span class="text-gray-900 fw-bolder fs-6">@{{ transfer ?? 0 }}kg</span>
                                                        <!--end::Number-->
                                                    </div>
                                                    <!--end::Statistics-->
                                                </div>
                                                <!--end::Item-->
                                            </div>
                                            <!--end::Body-->
                                        </div>
                                        <!--end::LIst widget 1-->
                                    </div>

                                    <div class="col-sm-12 mb-5 mb-xl-10">
                                        <!--begin::List widget 1-->
                                        <div class="card card-flush h-xl-100">
                                            <!--begin::Header-->
                                            <div class="card-header pt-7">
                                                <!--begin::Title-->
                                                <h3 class="card-title align-items-start flex-column">
                                                    <span class="card-label fw-bold text-gray-800">Status Logs</span>
                                                    <span class="text-uppercase mt-1 fs-8 fw-bolder" :class="'text-'+active_tank.cycle?.status.css">@{{ active_tank.cycle?.status.name }}</span>

                                                </h3>
                                                <!--end::Title-->
                                                <!--begin::Toolbar-->
                                                <div class="card-toolbar" v-if="false">
                                                    <a href="#" class="btn btn-sm btn-light" data-bs-toggle='tooltip' data-bs-dismiss='click' data-bs-custom-class="tooltip-inverse" title="Delivery App is coming soon">View All</a>
                                                </div>
                                                <!--end::Toolbar-->
                                            </div>
                                            <!--end::Header-->
                                            <!--begin::Body-->
                                            <div class="card-body pt-4 px-0">
                                             
                                                <div class="tab-content px-9 hover-scroll-overlay-y pe-7 me-3 mb-2" style="height: 254px">
                                                    <!--begin::Tap pane-->
                                                    <div class="tab-pane fade show active">
                                                        <!--begin::Item-->
                                                        <div class="m-0">
                                                            <!--begin::Timeline-->
                                                            <div class="timeline ms-n1">
                                                                <!--begin::Timeline item-->
                                                                <div class="timeline-item align-items-center mb-4" v-for="log in logs">
                                                                    <!--begin::Timeline line-->
                                                                    <div class="timeline-line w-20px mt-9 mb-n14"></div>
                                                                    <!--end::Timeline line-->
                                                                    <!--begin::Timeline icon-->
                                                                    <div class="timeline-icon pt-1" style="margin-left: 0.7px">
                                                                        <!--begin::Svg Icon | path: icons/duotune/general/gen015.svg-->
                                                                        <span class="svg-icon svg-icon-2" :class="'svg-icon-'+log?.status.css">
                                                                            <div v-html="log?.status.svg"></div>
                                                                        </span>
                                                                        <!--end::Svg Icon-->
                                                                    </div>
                                                                    <!--end::Timeline icon-->
                                                                    <!--begin::Timeline content-->
                                                                    <div class="timeline-content m-0">
                                                                        <!--begin::Label-->
                                                                        <span class="fs-8 fw-bolder text-uppercase" :class="'text-'+log?.status.css">@{{ log.status?.name }}</span>
                                                                        <!--begin::Label-->
                                                                        <!--begin::Title-->
                                                                        <a href="#" class="fs-6 text-gray-800 fw-bold d-block text-hover-primary">@{{ log.created_at | dateConvert }}</a>
                                                                        <!--end::Title-->
                                                                        <!--begin::Title-->
                                                                        <span class="fw-semibold text-gray-400">@{{ log.notes }}</span>
                                                                        <!--end::Title-->
                                                                    </div>
                                                                    <!--end::Timeline content-->
                                                                </div>
                                                                <!--end::Timeline item-->
                                                                <!--begin::Timeline item-->
                                                                <div class="timeline-item align-items-center" v-if="false">
                                                                    <!--begin::Timeline line-->
                                                                    <div class="timeline-line w-20px"></div>
                                                                    <!--end::Timeline line-->
                                                                    <!--begin::Timeline icon-->
                                                                    <div class="timeline-icon pt-1" style="margin-left: 0.5px">
                                                                        <!--begin::Svg Icon | path: icons/duotune/general/gen018.svg-->
                                                                        <span class="svg-icon svg-icon-2 svg-icon-info">
                                                                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                                <path opacity="0.3" d="M18.0624 15.3453L13.1624 20.7453C12.5624 21.4453 11.5624 21.4453 10.9624 20.7453L6.06242 15.3453C4.56242 13.6453 3.76242 11.4453 4.06242 8.94534C4.56242 5.34534 7.46242 2.44534 11.0624 2.04534C15.8624 1.54534 19.9624 5.24534 19.9624 9.94534C20.0624 12.0453 19.2624 13.9453 18.0624 15.3453Z" fill="currentColor" />
                                                                                <path d="M12.0624 13.0453C13.7193 13.0453 15.0624 11.7022 15.0624 10.0453C15.0624 8.38849 13.7193 7.04535 12.0624 7.04535C10.4056 7.04535 9.06241 8.38849 9.06241 10.0453C9.06241 11.7022 10.4056 13.0453 12.0624 13.0453Z" fill="currentColor" />
                                                                            </svg>
                                                                        </span>
                                                                        <!--end::Svg Icon-->
                                                                    </div>
                                                                    <!--end::Timeline icon-->
                                                                    <!--begin::Timeline content-->
                                                                    <div class="timeline-content m-0">
                                                                        <!--begin::Label-->
                                                                        <span class="fs-8 fw-bolder text-info text-uppercase">Receiver</span>
                                                                        <!--begin::Label-->
                                                                        <!--begin::Title-->
                                                                        <a href="#" class="fs-6 text-gray-800 fw-bold d-block text-hover-primary">Ralph Edwards</a>
                                                                        <!--end::Title-->
                                                                        <!--begin::Title-->
                                                                        <span class="fw-semibold text-gray-400">2464 Royal Ln. Mesa, New Jersey 45463</span>
                                                                        <!--end::Title-->
                                                                    </div>
                                                                    <!--end::Timeline content-->
                                                                </div>
                                                                <!--end::Timeline item-->
                                                            </div>
                                                            <!--end::Timeline-->
                                                        </div>
                                                      
                                                        <!--end::Item-->
                                                        <!--begin::Separator-->
                                                        {{-- <div class="separator separator-dashed mt-5 mb-4"></div> --}}
                                                        
                                                        <!--end::Item-->
                                                    </div>
                                                 
                                                </div>
                                                <!--end::Tab Content-->
                                            </div>
                                            <!--end: Card Body-->
                                        </div>
                                        <!--end::LIst widget 1-->
                                    </div>
                                   
                                </div>
                                
                            </div>
                           
                        </div>
                    </div>
                    <!--end::Col-->
                    <!--begin::Col-->
                    <div class="col-xl-8">
                        <!--begin::Table Widget 5-->
                        {{-- h-xl-100 --}}
                        <div class="card card-flush">
                            <!--begin::Card header-->
                            <div class="card-header pt-7">
                                <!--begin::Title-->
                                <h3 class="card-title align-items-start flex-column">
                                    <span class="card-label fw-bold text-dark">Probiotic Report</span>
                                    <span class="text-gray-400 mt-1 fw-semibold fs-6">Total feed consumed: <strong>@{{ total_quantity ?? 0 }}kg</strong></span>
                                </h3>
                                <!--end::Title-->
                                <!--begin::Actions-->
                                <div class="card-toolbar">
                                    <!--begin::Filters-->
                                    <div class="d-flex flex-stack flex-wrap gap-4">
                                        
                                        <!--end::Destination-->
                                        <!--begin::Status-->
                                        <div class="d-flex align-items-center fw-bold">
                                            <!--begin::Label-->
                                            <div class="text-muted fs-7 me-2">Status</div>
                                            <!--end::Label-->
                                            <!--begin::Select-->
                                            <select
                                                class="form-select form-select-transparent text-dark fs-7 lh-1 fw-bold py-0 ps-3 w-auto select2-hidden-accessible"
                                                data-control="select2" data-hide-search="true" data-dropdown-css-class="w-150px"
                                                data-placeholder="Select an option" data-kt-table-widget-5="filter_status"
                                                data-select2-id="select2-data-16-dx69" tabindex="-1" aria-hidden="true"
                                                data-kt-initialized="1">
                                                <option></option>
                                                <option value="Show All" selected="selected" data-select2-id="select2-data-18-6ca9">Show
                                                    All</option>
                                                <option value="In Stock">In Stock</option>
                                                <option value="Out of Stock">Out of Stock</option>
                                                <option value="Low Stock">Low Stock</option>
                                            </select><span class="select2 select2-container select2-container--bootstrap5" dir="ltr"
                                                data-select2-id="select2-data-17-xj4a" style="width: 100%;"><span
                                                    class="selection"><span
                                                        class="select2-selection select2-selection--single form-select form-select-transparent text-dark fs-7 lh-1 fw-bold py-0 ps-3 w-auto"
                                                        role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0"
                                                        aria-disabled="false" aria-labelledby="select2-q3lg-container"
                                                        aria-controls="select2-q3lg-container"><span class="select2-selection__rendered"
                                                            id="select2-q3lg-container" role="textbox" aria-readonly="true"
                                                            title="Show All">Show All</span><span class="select2-selection__arrow"
                                                            role="presentation"><b role="presentation"></b></span></span></span><span
                                                    class="dropdown-wrapper" aria-hidden="true"></span></span>
                                            <!--end::Select-->
                                        </div>
                                        <!--end::Status-->
                                        <!--begin::Search-->
                                        <a href="#"
                                            class="btn btn-light btn-sm" data-bs-toggle="modal" data-bs-target="#add_log" @click="addLogView('probiotics')">
                                        <!--end::Search-->Add new log
                                    </a>
                                    </div>
                                    <!--begin::Filters-->
                                </div>
                                <!--end::Actions-->
                            </div>
                            <!--end::Card header-->
                            <!--begin::Card body-->
                            <div class="card-body">
                                <!--begin::Table-->
                                <div id="kt_table_widget_5_table_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                                    <div class="table-responsive mb-4">
                                        <table class="table align-middle table-row-dashed fs-6 gy-3 dataTable no-footer"
                                            id="kt_table_widget_5_table">
                                            <!--begin::Table head-->
                                            <thead>
                                                <!--begin::Table row-->
                                                <tr class="text-start text-gray-400 fw-bold fs-7 text-uppercase gs-0">
                                                  
                                                    <th class="min-w-50px sorting">Log ID</th>
                                                    {{-- <th class="text-start min-w-70px sorting">Status</th> --}}
                                                  
                                                    <th class="text-end min-w-70px sorting">Quantity</th>
                                                    <th class="text-end min-w-100px sorting">Notes</th>
                                                    <th class="text-end min-w-100px sorting">Date Added</th>
                                                    {{-- <th class="text-end min-w-100px sorting_disabled" rowspan="1" colspan="1"
                                                        aria-label="Actions">
                                                        Actions</th> --}}
                                                </tr>
                                            </thead>
                                            <!--end::Table head-->
                                            <!--begin::Table body-->
                                            <tbody class="fw-bold text-gray-600">
                
                
                                                <tr class="" :class="{'even': index % 2 == 0, 'odd': index % 2 != 0 }"
                                                    v-for="(batch, index) in batches.data">
                                                    <!--begin::Checkbox-->
                                                  
                                                    <!--end::Checkbox-->
                                                    <!--begin::Order ID=-->
                                                    <td data-kt-ecommerce-order-filter="order_id">
                                                        <a href="../../demo1/dist/apps/ecommerce/sales/details.html"
                                                            class="text-gray-800 text-hover-primary fw-bold">@{{ batch.id }}</a>
                                                    </td>
                                                    <!--end::Order ID=-->
                                                    <!--begin::Customer=-->
                
                                                    <!--end::Customer=-->
                                                    <!--begin::Status=-->
                                                    <td class="text-start pe-0" data-order="Completed" v-if="false">
                                                        <!--begin::Badges-->
                                                        <div class="badge py-3 px-4 fs-7 badge-light-dark text-uppercase">
                                                            @{{ batch.type?.name }}</div>
                                                        <!--end::Badges-->
                                                    </td>
                
                                                 
                                                    <!--end::Status=-->
                                                    <!--begin::Total=-->
                                                    <td class="text-end pe-0">
                                                        <span class="fw-bold">@{{ batch.quantity }}</span>
                                                    </td>
                                                    <!--end::Total=-->
                                                    <!--begin::Date Added=-->
                                                    <td class="text-end" data-order="2022-06-27">
                                                        <span class="fw-bold">@{{ batch.notes }}</span>
                                                    </td>
                                                    <!--end::Date Added=-->
                                                    <!--begin::Date Modified=-->
                                                    <td class="text-end" data-order="2022-07-04">
                                                        <span class="fw-bold">@{{ batch.created_at | dateConvert}}</span>
                                                    </td>
                                                    <!--end::Date Modified=-->
                                                    <!--begin::Action=-->
                                                    <td class="text-end" v-if="false">
                                                        <a href="#" class="btn btn-sm btn-light btn-active-light-primary"
                                                            data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">Actions
                                                            <!--begin::Svg Icon | path: icons/duotune/arrows/arr072.svg-->
                                                            <span class="svg-icon svg-icon-5 m-0">
                                                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                                                    xmlns="http://www.w3.org/2000/svg">
                                                                    <path
                                                                        d="M11.4343 12.7344L7.25 8.55005C6.83579 8.13583 6.16421 8.13584 5.75 8.55005C5.33579 8.96426 5.33579 9.63583 5.75 10.05L11.2929 15.5929C11.6834 15.9835 12.3166 15.9835 12.7071 15.5929L18.25 10.05C18.6642 9.63584 18.6642 8.96426 18.25 8.55005C17.8358 8.13584 17.1642 8.13584 16.75 8.55005L12.5657 12.7344C12.2533 13.0468 11.7467 13.0468 11.4343 12.7344Z"
                                                                        fill="currentColor"></path>
                                                                </svg>
                                                            </span>
                                                            <!--end::Svg Icon--></a>
                                                        <!--begin::Menu-->
                                                        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-semibold fs-7 w-125px py-4"
                                                            data-kt-menu="true">
                                                            <!--begin::Menu item-->
                                                            <div class="menu-item px-3">
                                                                <a href="../../demo1/dist/apps/ecommerce/sales/details.html"
                                                                    class="menu-link px-3">View</a>
                                                            </div>
                                                            <!--end::Menu item-->
                                                            <!--begin::Menu item-->
                                                            <div class="menu-item px-3">
                                                                <a href="../../demo1/dist/apps/ecommerce/sales/edit-order.html"
                                                                    class="menu-link px-3">Edit</a>
                                                            </div>
                                                            <!--end::Menu item-->
                                                            <!--begin::Menu item-->
                                                            <div class="menu-item px-3">
                                                                <a href="#" class="menu-link px-3"
                                                                    data-kt-ecommerce-order-filter="delete_row">Delete</a>
                                                            </div>
                                                            <!--end::Menu item-->
                                                        </div>
                                                        <!--end::Menu-->
                                                    </td>
                                                    <!--end::Action=-->
                                                </tr>
                
                
                
                
                                            </tbody>
                                            <!--end::Table body-->
                                        </table>
                                    </div>
                                    <div class="row">
                                        <div
                                            class="col-sm-12 col-md-5 d-flex align-items-center justify-content-center justify-content-md-start">
                                        </div>
                                        <div class="col-sm-12 col-md-7 d-flex align-items-center justify-content-center justify-content-md-end">
                                            <ul class="pagination">
                                                {{-- <li v-for="(link, index) in logs.links" v-if="index != 0 && index != (logs.links.length - 1) && Math.abs(link.label - currentPage) < 3 || link.label == totalPages - 1 || link.label == 0">
                                                    <a href="#" @click="setPage(pageNumber)"  
                                                    :class="{current: currentPage === link.label, last: (link.label == totalPages - 1 && Math.abs(link.label - currentPage) > 3), first:(link.label == 0 && Math.abs(link.label - currentPage) > 3)}">
                                                    {{ link.label+1 }}</a>
                                                </li> --}}
                                                <li class="paginate_button page-item previous" :class="{'disabled': batches.current_page == 1}">
                                                        
                                                    <a href="javascript:;" class="page-link" @click="batches.current_page -= 1, viewBatches()">
                                                        <i class="previous"></i>
                                                    </a>
                                                   
                                                </li>
                                                <template>
                                                    
                                                    <li v-for="(link, index) in batches.links" class="paginate_button page-item " :class="{'active': link.active }">
                                                         <a href="javascript:;" class="page-link"  v-if="Math.abs(link.label - batches.current_page) < 3 || link.label == batches.links.length  || link.label == 0" @click="batches.current_page = parseInt(link.label), viewBatches()">@{{ link.label }}</a>
                                                         <span class="dot-sep" v-else></span>
                                                    </li>
                                                </template>
            
                                                <li class="paginate_button page-item next" :class="{'disabled': batches.current_page == batches.links?.length}">
                                                        
                                                      
                                                    <a href="javascript:;" class="page-link"  @click="batches.current_page += 1, viewBatches()">
                                                        <i class="next"></i>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <!--end::Table-->
                            </div>
                            <!--end::Card body-->
                        </div>
                        <!--end::Table Widget 5-->
                    </div>
                    <!--end::Col-->
                </div>
            
            </div>
            <!--end::Content container-->
        </div>
    </template>
    @include("modals.add_log")
    
    <!--end::Content-->
</div>
@endsection

@section('components')

@include('scripts.vue-components.select2')

@include('scripts.vue-components.dropdown')

<script>
    
    Vue.component('pagecomponent', {
        data: function () {
            return {
              tanks: [],
              active_tank: null,
              cycle_statuses: [],
              batch_statuses: [],
              total_quantity: 0,
              total_harvest: 0,
              logs: [],
              probiotics: {},
              transfer: 0,
              total_partial_harvest: 0,
              batch_type: null,
              batches: {
                current_page: 1,
              },
              type_name: 0,
              batch: {
                option: 'shirmp',
                type: 1,
                quantity: 0,
              },
              is_update: false,
              colors: ["bg-info", "bg-primary", "bg-danger", "bg-warning", "bg-success", "bg-dark"],
            }
        },
        props: [],
        template: `@yield('sub_component')`,
        filters: {
            dateConvert(value){
                return moment(value).format('LLL');
            },
            
        },

        watch: {
          'type_name': function(val){
            this.batch.type = this.batch_statuses[val].id;
            this.batch.name = this.batch_statuses[val].name;
           
          },
        },

        computed: {
            mapCycleStatuses(){
                
                return this.cycle_statuses.map(d => d.name);
            }
        },

        async mounted() {
            this.fetchTanks();
            
        },
        methods: {
            updateStatus(event, tank){
                
                var self = this;

           
                Swal.fire({
                    title: 'Are you sure to update tank status?',
                    input: 'text',
                    inputLabel: 'You can add your notes here',
                    showCancelButton: true,
                    confirmButtonText: 'Update Status',
                    inputValidator: (value) => {
                        console.log(value);
                    }
                }).then((result) => {

                    /* Read more about isConfirmed, isDenied below */
                    if(result.isConfirmed) {

                        self.buttonProcess(event, "on");
                        let obj = {
                            cycle_id: tank.active_cycle,
                            status_id: tank.cycle.status.id,
                            sort: tank.cycle.status.sort,
                            notes: result.value
                        }

                        axios.post('{{ route('tank.status.update') }}', obj)
                        .then(async res => {
                        if(res.data.success) {
                                self.buttonProcess(event, "off");
                                self.fetchTanks();
                                Swal.fire('Updated!', '','success');
                        } else {
                                Swal.fire(res.data.message, '','error');
                        }
                        })
                        .catch(err => {

                            self.errors = err.response.data.errors;
                            self.buttonProcess(event, "off");
                        })
                    }
                })
               
            },
            async addLogView(option){
                this.batch.option = option;
                this.probiotics = await axios.get('{{ route('warehouse.fetch.probiotics') }}').then(res => { return res.data });
               
            },

            viewBatches: _.debounce(function () {
               
                let obj = { 
                    current_page: this.batches.current_page,
                    // search: this.search,
                    // tags: this.tagFilter
                }
                this.batches.data = [];
                
                axios.get('{{ route('batch.log.fetch', '') }}'+'/'+this.active_tank.active_cycle, { params: obj})
                .then(res => {
                    var logs = res.data.batches;
                    this.total_quantity = res.data.total_quantity;
                    this.total_harvest = res.data.total_harvest;
                    this.total_partial_harvest = res.data.total_partial_harvest;
                    this.transfer = res.data.transfer;
                    this.logs = res.data.logs;
                    
                    logs.links.shift();
                    logs.links.pop();
                    this.batches = logs;
                    
                    console.log(this.batches);
                })
                .catch(err => {
                    console.error(err); 
                })

            }, 500),
            addBatch(event){

                this.buttonProcess(event, "on");

                
                this.batch.cycle_id = this.active_tank.active_cycle;

                if(this.batch.option == "probiotics") {

                    if(this.batch.quantity > this.probiotics?.quantity) {
                        Swal.fire("Exceeded Current Quantity", '','error');
                        this.buttonProcess(event, "off");
                        return;
                    }
                    this.batch.warehouse_id = this.probiotics.id;
                    this.batch.log_type = "minus";
                    this.batch.current_stock= this.probiotics.quantity;
                }

                console.log(this.batch);
                
                axios.post('{{ route('batch.log.create') }}', this.batch)
                .then(async res => {
                   if(res.data.success) {
                        this.buttonProcess(event, "off");
                        $("#add_log").modal("toggle");
                        this.batch = {
                            option: 'shirmp',
                            quantity: 0,
                            type: 1
                        }
                        this.viewBatches();
                        Swal.fire('Updated!', '','success');
                   } else {
                        Swal.fire(res.data.message, '','error');
                   }
                })
                .catch(err => {

                    this.errors = err.response.data.errors;
                    this.buttonProcess(event, "off");
                })
                
           
            },
            update(event, tank){
                this.buttonProcess(event, "on");
                this.active_tank = tank;
                this.is_update = true; 
                this.viewBatches();
            },
            statusSearch(data){
                var status = this.cycle_statuses.filter((s) => s.id == (data.status.id + 1));
                if(status.length == 0) return 'tset';
                return status[0].name;
            },
            fetchTanks(){
                axios.get('{{ route('tank.fetch') }}')
                .then(res => {
                    this.tanks = res.data;
                    this.fetchCycleStatuses();
                    this.fetchBatchStatuses();
                })
                .catch(err => {
                    console.error(err); 
                })
            },

            async fetchCycleStatuses(){
                this.cycle_statuses = await axios.get('{{ route('cycle.status.fetch') }}').then(res => { return res.data });
                console.log(this.cycle_statuses);
            },

            async fetchBatchStatuses(){
                this.batch_statuses = await axios.get('{{ route('batch.status.fetch') }}').then(res => { return res.data });
            },

            buttonProcess(event, status){
                var el = $(event.target);
                el.parents('.submit-button').find('.btn').attr("data-kt-indicator", status);
            },
            randomColor(){

                var items = ["bg-info", "bg-primary", "bg-danger", "bg-warning", "bg-success"];
                return items[Math.floor(Math.random()*items.length)];
            },
            createCycle(event, tank){
               
                Swal.fire({
                    html: `Are you sure you want to create a new cycle?`,
                    icon: "info",
                    buttonsStyling: false,
                    showCancelButton: true,
                    confirmButtonText: "Continue",
                    cancelButtonText: 'Cancel',
                    customClass: { 
                        confirmButton: "btn btn-primary",
                        cancelButton: 'btn btn-danger'
                    }
                }).then((confirm) => {
                    if (confirm.isConfirmed) {

                       axios.get('{{ route('cycle.create') }}', {params: tank})
                       .then(res => {
                            this.fetchTanks();
                            this.buttonProcess(event, "off");
                       })
                       .catch(err => {
                            console.error(err); 
                       })
                    } 

                    this.buttonProcess(event, "off");
                });
            }

        }
          
    })

</script>


@endsection
