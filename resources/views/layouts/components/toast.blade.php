<div class="position-fixed bottom-0 end-0 p-3" style="z-index: 9999;">
    <div id="kt_docs_toast_toggle" class="toast text-white bg-primary" role="alert" aria-live="assertive" aria-atomic="true">
        <div class="toast-header">
            <span class="svg-icon svg-icon-2 svg-icon-primary me-3">...</span>
            <strong class="me-auto">Keenthemes</strong>
            <small>11 mins ago</small>
            <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
        </div>
        <div class="toast-body">
            Hello, world! This is a toast message.
        </div>
    </div>
</div>