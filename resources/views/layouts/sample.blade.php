@extends('layouts.app')
@php
    $user = Auth::user();
@endphp

@section('content')
    <pagecomponent></pagecomponent>
@endsection

@section('sub_component')
    
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
      
        <div class="post d-flex flex-column-fluid" id="kt_post">
            <!--begin::Container-->
            <div id="kt_content_container" class="container">
             
            </div>
            <!--end::Container-->
        </div>
        <!--end::Post-->
    </div>
@endsection

@section('components')

@include('scripts.vue-components.select2')


<script>
    
    Vue.component('pagecomponent', {
        data: function () {
            return {
              
            }
        },
        props: [],
        template: `@yield('sub_component')`,
        filters: {
            dateConvert(value){
                return moment(value).format('LLL');
            },
            
        },

        watch: {
          
        },

        computed: {

        },

        async mounted() {
         
        },
        methods: {


        }
          
    })

</script>


@endsection
