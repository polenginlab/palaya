
@extends('layouts.app')

@section('content')
    <pagecomponent></pagecomponent>
@endsection

@section('sub_component')
    
    <div class="d-flex flex-column flex-column-fluid">
        <div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
            <!--begin::Toolbar container-->
            <div id="kt_app_toolbar_container" class="app-container container-xxl d-flex flex-stack">
                <!--begin::Page title-->
                <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                    <!--begin::Title-->
                    <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">Warehouse Inventory</h1>
                    <!--end::Title-->
                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1" v-if="false">
                        <!--begin::Item-->
                        <li class="breadcrumb-item text-muted">
                            <a href="../../demo1/dist/index.html" class="text-muted text-hover-primary">Home</a>
                        </li>
                        <!--end::Item-->
                        <!--begin::Item-->
                        <li class="breadcrumb-item">
                            <span class="bullet bg-gray-400 w-5px h-2px"></span>
                        </li>
                        <!--end::Item-->
                        <!--begin::Item-->
                        <li class="breadcrumb-item text-muted">eCommerce</li>
                        <!--end::Item-->
                        <!--begin::Item-->
                        <li class="breadcrumb-item">
                            <span class="bullet bg-gray-400 w-5px h-2px"></span>
                        </li>
                        <!--end::Item-->
                        <!--begin::Item-->
                        <li class="breadcrumb-item text-muted">Reports</li>
                        <!--end::Item-->
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page title-->
                <!--begin::Actions-->
                <div class="d-flex align-items-center gap-2 gap-lg-3">
                    <!--begin::Filter menu-->
                    <div class="m-0">
                        <!--begin::Menu toggle-->
                        <a href="#" class="btn btn-sm btn-flex bg-body btn-color-gray-700 btn-active-color-primary fw-bold" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">
                        <!--begin::Svg Icon | path: icons/duotune/general/gen031.svg-->
                        <span class="svg-icon svg-icon-6 svg-icon-muted me-1">
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M19.0759 3H4.72777C3.95892 3 3.47768 3.83148 3.86067 4.49814L8.56967 12.6949C9.17923 13.7559 9.5 14.9582 9.5 16.1819V19.5072C9.5 20.2189 10.2223 20.7028 10.8805 20.432L13.8805 19.1977C14.2553 19.0435 14.5 18.6783 14.5 18.273V13.8372C14.5 12.8089 14.8171 11.8056 15.408 10.964L19.8943 4.57465C20.3596 3.912 19.8856 3 19.0759 3Z" fill="currentColor" />
                            </svg>
                        </span>
                        <!--end::Svg Icon-->Filter</a>
                        <!--end::Menu toggle-->
                        <!--begin::Menu 1-->
                        <div class="menu menu-sub menu-sub-dropdown w-250px w-md-300px" data-kt-menu="true" id="kt_menu_62c3e333963a0">
                            <!--begin::Header-->
                            <div class="px-7 py-5">
                                <div class="fs-5 text-dark fw-bold">Filter Options</div>
                            </div>
                            <!--end::Header-->
                            <!--begin::Menu separator-->
                            <div class="separator border-gray-200"></div>
                            <!--end::Menu separator-->
                            <!--begin::Form-->
                            <div class="px-7 py-5">
                                <!--begin::Input group-->
                                <div class="mb-10">
                                    <!--begin::Label-->
                                    <label class="form-label fw-semibold">Status:</label>
                                    <!--end::Label-->
                                    <!--begin::Input-->
                                    <div>
                                        <select class="form-select form-select-solid" data-kt-select2="true" data-placeholder="Select option" data-dropdown-parent="#kt_menu_62c3e333963a0" data-allow-clear="true">
                                            <option></option>
                                            <option value="1">Approved</option>
                                            <option value="2">Pending</option>
                                            <option value="2">In Process</option>
                                            <option value="2">Rejected</option>
                                        </select>
                                    </div>
                                    <!--end::Input-->
                                </div>
                                <!--end::Input group-->
                                <!--begin::Input group-->
                                <div class="mb-10">
                                    <!--begin::Label-->
                                    <label class="form-label fw-semibold">Member Type:</label>
                                    <!--end::Label-->
                                    <!--begin::Options-->
                                    <div class="d-flex">
                                        <!--begin::Options-->
                                        <label class="form-check form-check-sm form-check-custom form-check-solid me-5">
                                            <input class="form-check-input" type="checkbox" value="1" />
                                            <span class="form-check-label">Author</span>
                                        </label>
                                        <!--end::Options-->
                                        <!--begin::Options-->
                                        <label class="form-check form-check-sm form-check-custom form-check-solid">
                                            <input class="form-check-input" type="checkbox" value="2" checked="checked" />
                                            <span class="form-check-label">Customer</span>
                                        </label>
                                        <!--end::Options-->
                                    </div>
                                    <!--end::Options-->
                                </div>
                                <!--end::Input group-->
                                <!--begin::Input group-->
                                <div class="mb-10">
                                    <!--begin::Label-->
                                    <label class="form-label fw-semibold">Notifications:</label>
                                    <!--end::Label-->
                                    <!--begin::Switch-->
                                    <div class="form-check form-switch form-switch-sm form-check-custom form-check-solid">
                                        <input class="form-check-input" type="checkbox" value="" name="notifications" checked="checked" />
                                        <label class="form-check-label">Enabled</label>
                                    </div>
                                    <!--end::Switch-->
                                </div>
                                <!--end::Input group-->
                                <!--begin::Actions-->
                                <div class="d-flex justify-content-end">
                                    <button type="reset" class="btn btn-sm btn-light btn-active-light-primary me-2" data-kt-menu-dismiss="true">Reset</button>
                                    <button type="submit" class="btn btn-sm btn-primary" data-kt-menu-dismiss="true">Apply</button>
                                </div>
                                <!--end::Actions-->
                            </div>
                            <!--end::Form-->
                        </div>
                        <!--end::Menu 1-->
                    </div>
                    <!--end::Filter menu-->
                    <!--begin::Secondary button-->
                    <!--end::Secondary button-->
                    <!--begin::Primary button-->
                    <a href="#" class="btn btn-sm fw-bold btn-primary" data-bs-toggle="modal" data-bs-target="#updatestocks">Update</a>
                    <!--end::Primary button-->
                </div>
                <!--end::Actions-->
            </div>
            <!--end::Toolbar container-->
        </div>
        <div id="kt_app_content" class="app-content flex-column-fluid">
            <!--begin::Content container-->
            <div id="kt_app_content_container" class="app-container container-xxl">
                <!--begin::Products-->
                <div class="card card-flush">
                    <!--begin::Card header-->
                    <div class="card-header align-items-center py-5 gap-2 gap-md-5">
                        <!--begin::Card title-->
                        <div class="card-title" v-if="false">
                            <!--begin::Search-->
                            <div class="d-flex align-items-center position-relative my-1">
                                <!--begin::Svg Icon | path: icons/duotune/general/gen021.svg-->
                                <span class="svg-icon svg-icon-1 position-absolute ms-4">
                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <rect opacity="0.5" x="17.0365" y="15.1223" width="8.15546" height="2" rx="1" transform="rotate(45 17.0365 15.1223)" fill="currentColor" />
                                        <path d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z" fill="currentColor" />
                                    </svg>
                                </span>
                                <!--end::Svg Icon-->
                                <input type="text" data-kt-ecommerce-order-filter="search" class="form-control form-control-solid w-250px ps-14" placeholder="Search Report" />
                            </div>
                            <!--end::Search-->
                            <!--begin::Export buttons-->
                            <div id="kt_ecommerce_report_returns_export" class="d-none"></div>
                            <!--end::Export buttons-->
                        </div>
                        <!--end::Card title-->
                        <!--begin::Card toolbar-->
                        <div class="card-toolbar flex-row-fluid justify-content-end gap-5">
                            <!--begin::Daterangepicker-->
                            <!--end::Daterangepicker-->
                            <!--begin::Export dropdown-->
                            <button type="button" class="btn btn-light-primary" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">
                            <!--begin::Svg Icon | path: icons/duotune/arrows/arr078.svg-->
                            <span class="svg-icon svg-icon-2">
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <rect opacity="0.3" x="12.75" y="4.25" width="12" height="2" rx="1" transform="rotate(90 12.75 4.25)" fill="currentColor" />
                                    <path d="M12.0573 6.11875L13.5203 7.87435C13.9121 8.34457 14.6232 8.37683 15.056 7.94401C15.4457 7.5543 15.4641 6.92836 15.0979 6.51643L12.4974 3.59084C12.0996 3.14332 11.4004 3.14332 11.0026 3.59084L8.40206 6.51643C8.0359 6.92836 8.0543 7.5543 8.44401 7.94401C8.87683 8.37683 9.58785 8.34458 9.9797 7.87435L11.4427 6.11875C11.6026 5.92684 11.8974 5.92684 12.0573 6.11875Z" fill="currentColor" />
                                    <path opacity="0.3" d="M18.75 8.25H17.75C17.1977 8.25 16.75 8.69772 16.75 9.25C16.75 9.80228 17.1977 10.25 17.75 10.25C18.3023 10.25 18.75 10.6977 18.75 11.25V18.25C18.75 18.8023 18.3023 19.25 17.75 19.25H5.75C5.19772 19.25 4.75 18.8023 4.75 18.25V11.25C4.75 10.6977 5.19771 10.25 5.75 10.25C6.30229 10.25 6.75 9.80228 6.75 9.25C6.75 8.69772 6.30229 8.25 5.75 8.25H4.75C3.64543 8.25 2.75 9.14543 2.75 10.25V19.25C2.75 20.3546 3.64543 21.25 4.75 21.25H18.75C19.8546 21.25 20.75 20.3546 20.75 19.25V10.25C20.75 9.14543 19.8546 8.25 18.75 8.25Z" fill="currentColor" />
                                </svg>
                            </span>
                            <!--end::Svg Icon-->Export Report</button>
                            <!--begin::Menu-->
                            <div id="kt_ecommerce_report_returns_export_menu" class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-semibold fs-7 w-200px py-4" data-kt-menu="true">
                                <!--begin::Menu item-->
                                <div class="menu-item px-3">
                                    <a href="#" class="menu-link px-3" data-kt-ecommerce-export="copy">Copy to clipboard</a>
                                </div>
                                <!--end::Menu item-->
                                <!--begin::Menu item-->
                                <div class="menu-item px-3">
                                    <a href="#" class="menu-link px-3" data-kt-ecommerce-export="excel">Export as Excel</a>
                                </div>
                                <!--end::Menu item-->
                                <!--begin::Menu item-->
                                <div class="menu-item px-3">
                                    <a href="#" class="menu-link px-3" data-kt-ecommerce-export="csv">Export as CSV</a>
                                </div>
                                <!--end::Menu item-->
                                <!--begin::Menu item-->
                                <div class="menu-item px-3">
                                    <a href="#" class="menu-link px-3" data-kt-ecommerce-export="pdf">Export as PDF</a>
                                </div>
                                <!--end::Menu item-->
                            </div>
                            <!--end::Menu-->
                            <!--end::Export dropdown-->
                        </div>
                        <!--end::Card toolbar-->
                    </div>
                    <!--end::Card header-->
                    <!--begin::Card body-->
                    <div class="card-body pt-0">
                        <!--begin::Table-->
                        <div id="kt_ecommerce_report_returns_table_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer"><div class="table-responsive">
                            <table class="table align-middle table-row-dashed fs-6 gy-5 dataTable no-footer" id="kt_ecommerce_report_returns_table">
                            <!--begin::Table head-->
                            <thead>
                                <!--begin::Table row-->
                                <tr class="text-start text-gray-400 fw-bold fs-7 text-uppercase gs-0">
                                    <th class="min-w-75px sorting" tabindex="0" aria-controls="kt_ecommerce_report_returns_table" rowspan="1" colspan="1" aria-label="Date: activate to sort column ascending" style="width: 100px;">Name</th>
                                    <th class="text-end min-w-75px sorting">Quantity</th>
                                    <th class="text-end min-w-75px sorting" >Last Update</th>
                                    <th class="text-end min-w-100px pe-5">Actions</th>
                                </tr>
                                <!--end::Table row-->
                            </thead>
                            <!--end::Table head-->
                            <!--begin::Table body-->
                            <tbody class="fw-semibold text-gray-600">
                                {{-- class="even" --}}
                                <tr class="odd" v-for="warehouse in warehouses">
                                    <!--begin::Date=-->
                                    <td>@{{ warehouse.name }}</td>
                                    <!--end::Date=-->
                                    <!--begin::No products returned=-->
                                    <td class="text-end pe-0" :class="{'text-danger': warehouse.quantity < 10, 'text-warning': warehouse.quantity < 50}">@{{ warehouse.quantity | formatPrice }} kg</td>
                                    <!--end::No products returned=-->
                                    <!--begin::No orders refunded=-->
                                    <td class="text-end pe-0">@{{ warehouse.updated_at | dateConvert }}</td>

                                    <td class="text-end"><a href="#" class="btn btn-sm btn-primary" data-bs-toggle="modal" data-bs-target="#viewlogs" @click="viewLogs(warehouse)">View Logs</a></td>
                                 
                                </tr>
                               
                            </tbody>
                            <!--end::Table body-->
                        </table></div><div class="row"><div class="col-sm-12 col-md-5 d-flex align-items-center justify-content-center justify-content-md-start"><div class="dataTables_length" id="kt_ecommerce_report_returns_table_length"><label><select name="kt_ecommerce_report_returns_table_length" aria-controls="kt_ecommerce_report_returns_table" class="form-select form-select-sm form-select-solid"><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select></label></div></div><div class="col-sm-12 col-md-7 d-flex align-items-center justify-content-center justify-content-md-end"><div class="dataTables_paginate paging_simple_numbers" id="kt_ecommerce_report_returns_table_paginate"><ul class="pagination"><li class="paginate_button page-item previous disabled" id="kt_ecommerce_report_returns_table_previous"><a href="#" aria-controls="kt_ecommerce_report_returns_table" data-dt-idx="0" tabindex="0" class="page-link"><i class="previous"></i></a></li><li class="paginate_button page-item active"><a href="#" aria-controls="kt_ecommerce_report_returns_table" data-dt-idx="1" tabindex="0" class="page-link">1</a></li><li class="paginate_button page-item "><a href="#" aria-controls="kt_ecommerce_report_returns_table" data-dt-idx="2" tabindex="0" class="page-link">2</a></li><li class="paginate_button page-item "><a href="#" aria-controls="kt_ecommerce_report_returns_table" data-dt-idx="3" tabindex="0" class="page-link">3</a></li><li class="paginate_button page-item "><a href="#" aria-controls="kt_ecommerce_report_returns_table" data-dt-idx="4" tabindex="0" class="page-link">4</a></li><li class="paginate_button page-item "><a href="#" aria-controls="kt_ecommerce_report_returns_table" data-dt-idx="5" tabindex="0" class="page-link">5</a></li><li class="paginate_button page-item next" id="kt_ecommerce_report_returns_table_next"><a href="#" aria-controls="kt_ecommerce_report_returns_table" data-dt-idx="6" tabindex="0" class="page-link"><i class="next"></i></a></li></ul></div></div></div></div>
                        <!--end::Table-->
                    </div>
                    <!--end::Card body-->
                </div>
                <!--end::Products-->
            </div>
            <!--end::Content container-->
        </div>

        <div class="modal fade" tabindex="-1" id="updatestocks">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title">Update stocks</h3>
        
                        <!--begin::Close-->
                        <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                            <span class="svg-icon svg-icon-1"></span>
                        </div>
                        <!--end::Close-->
                    </div>
        
                    <div class="modal-body">
                        <div class="card-body pt-0">
                            <!--begin::Input group-->
                            <div class="mb-5 fv-row fv-plugins-icon-container">
                                <!--begin::Label-->
                                <label class="required form-label">Stock Item</label>
                                <!--end::Label-->
                                <!--begin::Input-->
                                <select2 class="form-control" name="parent" :search="false" :multiple="false" placeholder="Select Book Name" modal="updatestocks" v-model="selectWarehouse">
                                    <option :value="index" v-for="(warehouse, index) in warehouses">@{{ warehouse.name }}</option>
                                </select2>
                                <!--end::Input-->
                                <div class="fv-plugins-message-container invalid-feedback"></div>
                            </div>
                            <!--end::Input group-->
                            <!--begin::Input group-->
                            <div class="mb-5 fv-row fv-plugins-icon-container">
                                <!--begin::Label-->
                                <label class="required form-label">Current Stocks</label>
                                <!--end::Label-->
                                <!--begin::Input-->
                                <input type="text" name="sku" class="form-control mb-2" placeholder="Barcode Number" disabled v-model="warehouse.quantity">
                                <!--end::Input-->
                                <!--begin::Description-->
                                <!--end::Description-->
                            <div class="fv-plugins-message-container invalid-feedback"></div></div>
                            <!--end::Input group-->
                            <!--begin::Input group-->
                            <div class="mb-5 fv-row fv-plugins-icon-container">
                                <!--begin::Label-->
                                <label class="required form-label">Quantity</label>
                                <!--end::Label-->
                                <!--begin::Input-->
                                <div class="d-flex gap-3">
                                    <input type="text" class="form-control mb-2" placeholder="Add Quantity" v-model="log.quantity">
                                    <select2 class="form-control mb-2" name="parent" :search="false" :multiple="false" placeholder="Select Book Name" modal="updatestocks" v-model="log.unit" :key="log.unit">
                                        <option value="KG" selected>KG</option>
                                        <option value="SACK">SACK</option>
                                    </select2>
                                </div>
                                <!--end::Input-->
                                <!--begin::Description-->
                                <div class="text-muted fs-7">Enter the product quantity.</div>
                                <!--end::Description-->

                                <div class="fv-plugins-message-container invalid-feedback" v-if="errors.quantity">
                                    <div data-field="email_input" data-validator="notEmpty">@{{ errors.quantity[0]  }}</div>
                                </div>
                            </div>
                            <!--end::Input group-->
                            <!--begin::Input group-->
                            <div class="mb-5 fv-row fv-plugins-icon-container">
                                <!--begin::Label-->
                                <label class="required form-label">Notes</label>
                                <!--end::Label-->
                                <!--begin::Input-->
                                <textarea class="form-control" name="" id="" cols="10" rows="5" v-model="log.notes"></textarea>
                               
                                <!--end::Input-->
                                <div class="fv-plugins-message-container invalid-feedback"></div>
                            </div>
                            <!--end::Input group-->
                        </div>
                    </div>
        
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" @click="saveLog()">Save changes</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" tabindex="-1" id="viewlogs">
            <div class="modal-dialog modal-dialog-scrollable">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">@{{ activeWarehouse?.name }}</h5>
        
                        <!--begin::Close-->
                        <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                            <span class="svg-icon svg-icon-2x"></span>
                        </div>
                        <!--end::Close-->
                    </div>
        
                    <div class="modal-body" v-infinite-scroll="loadMore" infinite-scroll-disabled="busy" infinite-scroll-distance="100">
                        <div class="card-body d-flex align-items-end">
                            <!--begin::Wrapper-->
                            <div class="w-100"  >
                               
                                <!--begin::Item-->
                                <template v-for="log in logs.data">
                                    <div class="d-flex align-items-center" >
                                       
                                        <div class="d-flex align-items-center flex-stack flex-wrap d-grid gap-1 flex-row-fluid">
                                            <!--begin::Content-->
                                            <div class="me-5">
                                                <!--begin::Title-->
                                                <a href="#" class="text-gray-800 fw-bold text-hover-primary fs-6">@{{ log.created_at | dateConvert }}</a>
                                                <!--end::Title-->
                                                <!--begin::Desc-->
                                                <span class="text-gray-400 fw-semibold fs-7 d-block text-start ps-0" style="max-width: 250px;">
                                                    <span v-if="log.notes">@{{ log.notes }}</span>
                                                    <i v-else>No notes</i>
                                                </span>
                                                <!--end::Desc-->
                                            </div>
                                            <!--end::Content-->
                                            <!--begin::Wrapper-->
                                            <div class="d-flex align-items-center">
                                                <!--begin::Number-->
                                                <span class="text-gray-800 fw-bold fs-4 me-3">@{{ parseInt(log.current_stock) + parseInt(log.quantity) | formatPrice }} kg</span>
                                                <!--end::Number-->
                                                <!--begin::Info-->
                                                <!--begin::label-->
                                                <span class="badge fs-base" :class="(log.quantity < 0) ? 'badge-light-danger' : 'badge-light-success'">
                                                    <span v-if="log.quantity > 0">+</span>
                                                    @{{ log.quantity | formatPrice }} kg
                                                </span>
                                                <!--end::label-->
                                                <!--end::Info-->
                                            </div>
                                            <!--end::Wrapper-->
                                        </div>
                                        <!--end::Container-->
                                    </div>
                                    <!--end::Item-->
                                    <!--begin::Separator-->
                                    <div class="separator separator-dashed my-3"></div>

                                </template>
                                <!--end::Separator-->
                                <!--begin::Item-->
                                <div class="d-flex align-items-center" v-if="false">
                                    <!--begin::Symbol-->
                                    <div class="symbol symbol-30px me-5">
                                        <span class="symbol-label">
                                            <!--begin::Svg Icon | path: icons/duotune/abstract/abs026.svg-->
                                            <span class="svg-icon svg-icon-3 svg-icon-gray-600">
                                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path opacity="0.3" d="M7 20.5L2 17.6V11.8L7 8.90002L12 11.8V17.6L7 20.5ZM21 20.8V18.5L19 17.3L17 18.5V20.8L19 22L21 20.8Z" fill="currentColor"></path>
                                                    <path d="M22 14.1V6L15 2L8 6V14.1L15 18.2L22 14.1Z" fill="currentColor"></path>
                                                </svg>
                                            </span>
                                            <!--end::Svg Icon-->
                                        </span>
                                    </div>
                                    <!--end::Symbol-->
                                    <!--begin::Container-->
                                    <div class="d-flex align-items-center flex-stack flex-wrap d-grid gap-1 flex-row-fluid">
                                        <!--begin::Content-->
                                        <div class="me-5">
                                            <!--begin::Title-->
                                            <a href="#" class="text-gray-800 fw-bold text-hover-primary fs-6">Other</a>
                                            <!--end::Title-->
                                            <!--begin::Desc-->
                                            <span class="text-gray-400 fw-semibold fs-7 d-block text-start ps-0">Many Sources</span>
                                            <!--end::Desc-->
                                        </div>
                                        <!--end::Content-->
                                        <!--begin::Wrapper-->
                                        <div class="d-flex align-items-center">
                                            <!--begin::Number-->
                                            <span class="text-gray-800 fw-bold fs-4 me-3">79,458</span>
                                            <!--end::Number-->
                                            <!--begin::Info-->
                                            <!--begin::label-->
                                            <span class="badge badge-light-success fs-base">
                                            <!--begin::Svg Icon | path: icons/duotune/arrows/arr066.svg-->
                                            <span class="svg-icon svg-icon-5 svg-icon-success ms-n1">
                                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <rect opacity="0.5" x="13" y="6" width="13" height="2" rx="1" transform="rotate(90 13 6)" fill="currentColor"></rect>
                                                    <path d="M12.5657 8.56569L16.75 12.75C17.1642 13.1642 17.8358 13.1642 18.25 12.75C18.6642 12.3358 18.6642 11.6642 18.25 11.25L12.7071 5.70711C12.3166 5.31658 11.6834 5.31658 11.2929 5.70711L5.75 11.25C5.33579 11.6642 5.33579 12.3358 5.75 12.75C6.16421 13.1642 6.83579 13.1642 7.25 12.75L11.4343 8.56569C11.7467 8.25327 12.2533 8.25327 12.5657 8.56569Z" fill="currentColor"></path>
                                                </svg>
                                            </span>
                                            <!--end::Svg Icon-->8.3%</span>
                                            <!--end::label-->
                                            <!--end::Info-->
                                        </div>
                                        <!--end::Wrapper-->
                                    </div>
                                    <!--end::Container-->
                                </div>
                                <!--end::Item-->
                                <!--begin::Link-->
                               

                                
                                <!--end::Link-->
                            </div>
                            <!--end::Wrapper-->
                        </div>
                    </div>
        
                    <div class="modal-footer">
                        <div class="text-center" style="width: 100%;">
                                   
                            <div id="kt_ecommerce_report_returns_table_paginate" class="dataTables_paginate paging_simple_numbers">
                                <ul class="pagination">
                                    {{-- <li v-for="(link, index) in logs.links" v-if="index != 0 && index != (logs.links.length - 1) && Math.abs(link.label - currentPage) < 3 || link.label == totalPages - 1 || link.label == 0">
                                        <a href="#" @click="setPage(pageNumber)"  
                                        :class="{current: currentPage === link.label, last: (link.label == totalPages - 1 && Math.abs(link.label - currentPage) > 3), first:(link.label == 0 && Math.abs(link.label - currentPage) > 3)}">
                                        {{ link.label+1 }}</a>
                                    </li> --}}
                                    <li class="paginate_button page-item previous" >
                                            
                                        <a href="javascript:;" class="page-link" @click="logs.current_page -= 1, viewLogs(warehouse)">
                                            <i class="previous"></i>
                                        </a>
                                       
                                    </li>
                                    <template>
                                        
                                        <li v-for="(link, index) in logs.links" class="paginate_button page-item " :class="{'active': link.active }">
                                             <a href="javascript:;" class="page-link"  v-if="Math.abs(link.label - logs.current_page) < 3 || link.label == logs.links.length  || link.label == 0" @click="logs.current_page = parseInt(link.label), viewLogs(warehouse)">@{{ link.label }}</a>
                                             <span class="dot-sep" v-else></span>
                                        </li>
                                    </template>

                                    <li class="paginate_button page-item next">
                                            
                                          
                                        <a href="javascript:;" class="page-link"  @click="logs.current_page += 1, viewLogs(warehouse)">
                                            <i class="next"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

       
    </div>
@endsection

@section('components')

@include('scripts.vue-components.select2')
<script>
    
    Vue.component('pagecomponent', {
        data: function () {
            return {
                stocksModal: false,
                busy: false,
                warehouses: [],
                activeWarehouse: null,
                logs: {
                    current_page: 1,
                    data: []
                },
                warehouse: {
                    quantity: null
                },
                current_page: 1,
                datas: {
                    current_page: 1,
                    data: []
                },
                search: null,
                tagFilter: null,
                log:{
                    unit: "KG",
                    log_type: "add",
                    notes: null,
                    quantity: 0
                },
                errors:{
                    quantity: null
                
                },
                selectWarehouse: null
            }
        },
        props: [],
        template: `@yield('sub_component')`,
        directives: {infiniteScroll},
        filters: {
            dateConvert(value){
                return moment(value).format('LLL');
            },
            formatPrice(value) {
                return Number(value).toLocaleString();
            },
        },

        watch: {
            selectWarehouse(index){
               this.updateWarehouse(index);
            }
        },

        computed: {

        },

        async mounted() {
            this.fetchWarehouse();
          
        },
        methods: {
            
            loadMore: function() {
               
            },
            fetchWarehouse(){
                axios.get('{{ route('warehouse.fetch') }}')
                .then(res => {
                    this.warehouses = res.data;

                    console.log(this.warehouses);
                    this.updateWarehouse(0);
                
                })
                .catch(err => {
                    console.error(err); 
                })
            },

            viewLogs: _.debounce(function (warehouse) {
                this.activeWarehouse = warehouse;
                
                let obj = { 
                    current_page: this.logs.current_page,
                    search: this.search,
                    tags: this.tagFilter
                }

                
                axios.get('{{ route('warehouse.logs.fetch', '') }}'+'/'+warehouse.id, { params: obj})
                .then(res => {
                    var logs = res.data;
                    logs.links.shift();
                    logs.links.pop();
                    this.logs = logs;
                    
                    console.log(this.logs);
                })
                .catch(err => {
                    console.error(err); 
                })

            }, 500),

            // viewLogs(warehouse){
               
            //     axios.get('{{ route('warehouse.logs.fetch', '') }}'+'/'+warehouse.id)
            //     .then(res => {
            //         this.logs  = res.data;
            //     })
            //     .catch(err => {
            //         console.error(err); 
            //     })
            // },

            updateWarehouse(index){

                this.warehouse = this.warehouses[index];
                this.log.warehouse_id = this.warehouse.id;
                this.log.current_stock = this.warehouse.quantity;
            },

            saveLog(){
                var obj = {};
                var self = this;
                
                Object.keys(this.log).map(function(key, index) { obj[key] = self.log[key]; });

                if(obj.unit == "SACK") obj.quantity = obj.quantity * 25;

                axios.post('{{ route('warehouse.log.create') }}', obj)
                .then(async res => {
                   if(res.data.success) {
                        await this.fetchWarehouse();
                        $("#updatestocks").modal("toggle");
                        this.log = {
                            unit: "KG",
                            log_type: "add",
                            notes: null,
                            quantity: 0
                        }
                        Swal.fire('Updated!', '','success');
                   } else {
                        Swal.fire(res.data.message, '','error');
                   }
                })
                .catch(err => {

                    this.errors = err.response.data.errors;
                    console.log(this.errors); 
                })
            },

        }
          
    })

</script>


@endsection
