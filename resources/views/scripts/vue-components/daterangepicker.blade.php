<script>
    Vue.component('daterangepicker', {
        data: function () {
            return {
                items: [],
            }
        },
        props: ['value'],
        template: `<input class="form-control form-control-solid" placeholder="Pick date rage"  :value="value"/>`,
        computed:{
        
        },
        mounted() {
            var start = moment().subtract(29, "days");
            var end = moment();
            var el = $(this.$el);
            var self = this;
            function cb(start, end) {
                el.html(start.format("MMMM D, YYYY") + " - " + end.format("MMMM D, YYYY"));
            }

            el.daterangepicker({
                startDate: start,
                endDate: end,
                ranges: {
                    "Today": [moment(), moment()],
                    "Yesterday": [moment().subtract(1, "days"), moment().subtract(1, "days")],
                    "Last 7 Days": [moment().subtract(6, "days"), moment()],
                    "Last 30 Days": [moment().subtract(29, "days"), moment()],
                    "This Month": [moment().startOf("month"), moment().endOf("month")],
                    "Last Month": [moment().subtract(1, "month").startOf("month"), moment().subtract(1, "month").endOf("month")]
                }
            }, cb).on('apply.daterangepicker', function(ev, picker){

                self.$emit('input', picker.startDate.format("L") + " - " + picker.endDate.format("L"));
            });

            cb(start, end);
        },

        watch: {
            
        },
        destroyed: function () {
            // $(this.$el).off().select2('destroy')
        },

        methods: {



        },
    })

    Vue.component('datepicker', {
        data: function () {
            return {
                items: [],
            }
        },
        props: ['value'],
        template: `<input class="form-control form-control-solid" placeholder="Pick date rage"  :value="value"/>`,
        computed:{
        
        },
        mounted() {
            var el = $(this.$el);
            var self = this;
            el.daterangepicker({
                    singleDatePicker: true,
                    showDropdowns: true,
                    minYear: 1901,
                    maxYear: parseInt(moment().format("YYYY"),10)

                }, function(start, end, label) {
                    self.$emit('input', start.format('L'));
                }
            );
        },

        watch: {
            
        },
        destroyed: function () {
            // $(this.$el).off().select2('destroy')
        },

        methods: {



        },
    })

</script>