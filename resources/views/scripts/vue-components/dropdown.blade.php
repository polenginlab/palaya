

<script>
    

    Vue.component('dropdown', {
        data: function () {
            return {
                items: []
            }
        },
        props: [],
        template: `<div class="">
            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Dropdown button
  </button>
  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
    <a class="dropdown-item" href="#">Action</a>
    <a class="dropdown-item" href="#">Another action</a>
    <a class="dropdown-item" href="#">Something else here</a>
  </div>
            </div>`,
        mounted() {
            $(this.$el).dropdown()
        },

        watch: {
           
        },
        destroyed: function () {
            
        },

        methods: {

        },
    })
</script>