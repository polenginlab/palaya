

<script>
    

    Vue.component('toast', {
        data: function () {
            return {
                items: []
            }
        },
        props: [],
        template: `<div class="position-fixed top-0 end-0 p-3 z-index-3">
                    <div id="kt_docs_toast_toggle" class="toast" role="alert" aria-live="assertive" aria-atomic="true">
                        <div class="toast-header">
                            <span class="svg-icon svg-icon-2 svg-icon-primary me-3">...</span>
                            <strong class="me-auto">Keenthemes</strong>
                            <small>11 mins ago</small>
                            <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
                        </div>
                        <div class="toast-body">
                            Hello, world! This is a toast message.
                        </div>
                    </div>
                </div>`,
        mounted() {
            var vm = this
           
        },

        watch: {
           
        },
        destroyed: function () {
            
        },

        methods: {

        },
    })
</script>