<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if(Auth::check()) {
        return view('home');
    } else {
        return view('auth.login');
    }
});

Auth::routes();

Route::get('/create/role/{type}', [App\Http\Controllers\RoleController::class, 'create'])->name('role.create');
Route::get('/assign/role/{id}/{role}', [App\Http\Controllers\RoleController::class, 'assign'])->name('role.assign');


Route::group(['middleware' => ['role:admin']], function () {
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

    // Tanks
    Route::get('/tanks/fetch', [App\Http\Controllers\TankController::class, 'fetch'])->name('tank.fetch');
    Route::post('/tanks/status/update', [App\Http\Controllers\TankController::class, 'updateStatus'])->name('tank.status.update');

    // Cycle
    Route::get('/cycle/create', [App\Http\Controllers\CycleController::class, 'create'])->name('cycle.create');
    Route::get('/cycle/status/fetch', [App\Http\Controllers\CycleController::class, 'fetchStatuses'])->name('cycle.status.fetch');

    // Batch
    Route::get('/batch/status/fetch', [App\Http\Controllers\BatchController::class, 'fetchStatuses'])->name('batch.status.fetch');
    Route::post('/batch/log/create', [App\Http\Controllers\BatchController::class, 'createBatch'])->name('batch.log.create');
    Route::get('/batch/log/fetch/{id}', [App\Http\Controllers\BatchController::class, 'fetchBatches'])->name('batch.log.fetch');

    // Warehouse
    Route::get('/warehouse', function(){
        return view("pages.warehouse");
    })->name('warehouse');

    Route::get('/warehouse/fetch', [App\Http\Controllers\WarehouseController::class, 'fetch'])->name('warehouse.fetch');
    Route::get('/warehouse/fetch/probiotics', [App\Http\Controllers\WarehouseController::class, 'probiotics'])->name('warehouse.fetch.probiotics');
    Route::post('/warehouse/log/create', [App\Http\Controllers\WarehouseController::class, 'createLog'])->name('warehouse.log.create');
    
    // Warehouse logs
    Route::get('/warehouse/logs/fetch/{id}', [App\Http\Controllers\WarehouseController::class, 'logs'])->name('warehouse.logs.fetch');


});
